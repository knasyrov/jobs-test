class AddIsActiveToCandidates < ActiveRecord::Migration
  def change
    add_column :candidates, :is_active, :boolean, default: true
  end
end
