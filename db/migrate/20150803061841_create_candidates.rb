class CreateCandidates < ActiveRecord::Migration
  def change
    create_table :candidates do |t|
      t.string :name
      t.string :contacts
      t.decimal :salary, :precision => 10, :scale => 2
      t.integer :skill_ids, array: true, default: '{}'

      t.timestamps null: false
    end
  end
end
