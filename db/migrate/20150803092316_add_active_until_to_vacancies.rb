class AddActiveUntilToVacancies < ActiveRecord::Migration
  def change
    add_column :vacancies, :active_until, :date
  end
end
