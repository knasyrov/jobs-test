class CreateVacancies < ActiveRecord::Migration
  def change
    create_table :vacancies do |t|
      t.string :name
      t.string :contacts
      t.decimal :wage, :precision => 10, :scale => 2
      t.integer :skill_ids, array: true, default: '{}'

      t.timestamps null: false
    end
  end
end
