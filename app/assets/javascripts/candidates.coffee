# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/


$(document).on 'page:load ready', ->
  $('.btn-remove-skill').on 'click', ->
    $(@).closest('tr').fadeOut 'slow', ->
      $(@).empty()
      return

  $('.btn-add-skill-candidate').on 'click', (e)->
    skill_id = $('#skills_manager input#selected_skill').val()
    skill_name = $('#skill_name').val()
    if skill_id == ''
      new_row = "<tr><td><input type='hidden' name='candidate[new_skills][]' id='candidate_skill_ids_' value='#{skill_name}'>#{skill_name}</td><td>
        <a class='btn btn-xs btn-danger btn-remove-skill' href='#'><span class='glyphicon glyphicon-remove'></span></a></td></tr>"
    else
      new_row = "<tr><td><input type='hidden' name='candidate[skill_ids][]' id='candidate_skill_ids_' value='#{skill_id}'>#{skill_name}</td><td>
        <a class='btn btn-xs btn-danger btn-remove-skill' href='#'><span class='glyphicon glyphicon-remove'></span></a></td></tr>"
    $('#skills_manager table').append(new_row)
    e.preventDefault()
    e.stopPropagation() 


  $('.btn-add-skill-vacancy').on 'click', (e)->
    skill_id = $('#skills_manager input#selected_skill').val()
    skill_name = $('#skill_name').val()
    if skill_id == ''
      new_row = "<tr><td><input type='hidden' name='vacancy[new_skills][]' id='vacancy_skill_ids_' value='#{skill_name}'>#{skill_name}</td><td>
        <a class='btn btn-xs btn-danger btn-remove-skill' href='#'><span class='glyphicon glyphicon-remove'></span></a></td></tr>"
    else
      new_row = "<tr><td><input type='hidden' name='vacancy[skill_ids][]' id='vacancy_skill_ids_' value='#{skill_id}'>#{skill_name}</td><td>
        <a class='btn btn-xs btn-danger btn-remove-skill' href='#'><span class='glyphicon glyphicon-remove'></span></a></td></tr>"          
    $('#skills_manager table').append(new_row)
    e.preventDefault()
    e.stopPropagation() 


  $('#skill_name').on 'keydown', ->
    $('#skills_manager input#selected_skill').val('')
 