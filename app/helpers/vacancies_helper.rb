module VacanciesHelper
  def vacancy_periods
    periods = []
    periods += (1..6).map {|d| ["#{d} #{Russian::p(d, 'день', 'дня', 'дней')}", "#{d}.day"]}
    periods += (1..3).map {|d| ["#{d} #{Russian::p(d, 'неделя', 'недели', 'недель')}", "#{d}.week"]}
    periods += (1..2).map {|d| ["#{d} #{Russian::p(d, 'месяц', 'месяца', 'месяцев')}", "#{d}.month"]}
    periods
  end
end
