module SkillSupport
  extend ActiveSupport::Concern

  included do
    validate :check_skills
    before_save :build_skills

    attr_reader :new_skills
  end

  def new_skills= skills
    @new_skills = skills.select {|a| a.present?}
  end  

  def build_skills
    if @new_skills && @new_skills.any?
      @new_skills.each do |name|
        begin
          new_skill = Skill.create!(name: name)
          self.skill_ids += [new_skill.id]
        rescue ActiveRecord::RecordInvalid
          skill_id = Skill.like_name(name).first.try(:id)
          self.skill_ids |= [skill_id]
        end
      end
    end
    self.skill_ids.compact!
  end

  def check_skills
    skills = self.skill_ids.compact
    new_skills = @new_skills || []
    if skills.empty? && new_skills.empty?
      self.errors.add(:base, 'Навыки должны быть заданы')
    end
  end

end