class Vacancy < ActiveRecord::Base
  include SkillSupport

  validates :name, :contacts, presence: true
  validates :wage, numericality: {greater_than: 0}

  scope :by_skill, ->(skill_id) {where("(array_remove(skill_ids, NULL) <> '{}') and (intset(?) && array_remove(skill_ids, NULL))", skill_id)}
  scope :by_skills, ->(skill_ids) {where("(array_remove(skill_ids, NULL) <> '{}') and (icount(array_remove(skill_ids, NULL) - ARRAY[?]) = 0)", skill_ids)}
  #scope :by_any_skills, ->(skill_ids) {where("icount(ARRAY[?] & skill_ids) > 0", skill_ids)}
  scope :by_wage, -> {order('wage desc')}
  scope :active, -> {where("active_until > ?", Date.today)}

  def skills
    Skill.where(id: self.skill_ids)
  end

  def best_candidates
    skills = self.skill_ids.compact
    skills = [0] if !skills || skills.empty? 
    Candidate.by_skills(skills).active.by_salary
  end

  def other_candidates
    skills = self.skill_ids.compact
    skills = [0] if !skills || skills.empty? 
    Candidate.active.
      select("candidates.*, count(skills.id) common_skill_count, string_agg(skills.name, ', ') skill_names").
      joins("inner join skills on skills.id = ANY(skill_ids & ARRAY#{skills})").
      group('candidates.id').having("count(skills.id) between 1 and #{skills.count-1}").order('count(skills.id) desc').by_salary
  end

  def validity= period
    @validity = period
    q = period.match(/(\d*).(days|day|week|weeks|month|months)/).captures
    self.active_until = Date.today + q[0].to_i.send(q[1])
  end

  def validity
    @validity
  end
end
