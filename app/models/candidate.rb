class Candidate < ActiveRecord::Base
  include SkillSupport

  validates :name, format: {with: /\A\s*\p{Cyrillic}+\s+\p{Cyrillic}+\s+\p{Cyrillic}+\s*\z/, message: 'должно содержать 3 слова и только кириллические буквы и пробелы'}
  validates :salary, numericality: {greater_than: 0}
  validate :check_contacts_field

  scope :by_skill, ->(skill_id) {where("(array_remove(skill_ids, NULL) <> '{}') and (intset(?) && array_remove(skill_ids, NULL))", skill_id)}
  scope :by_skills, ->(skill_ids) {where("array_remove(skill_ids, NULL) <> '{}' and (icount(ARRAY[?] - array_remove(skill_ids, NULL)) = 0)", skill_ids)}
  #scope :by_any_skills, ->(skill_ids) {where("icount(ARRAY[?] & skill_ids) > 0", skill_ids)}
  scope :by_salary, -> {order :salary}
  scope :active, -> {where(is_active: true)}


  def check_contacts_field
    unless self.contacts.present?
      self.errors.add(:contacts, 'должны быть заданы') 
    else
      cnt = self.contacts || ''
      cnt.gsub!(/\s/, '')
      unless cnt =~ /\A[^@\s]+@([^@.\s]+\.)+[^@.\s]+\z/  # мыло
        phone = cnt.gsub(/\s|-|_|\(|\)/, '')
        unless phone =~ /\A\+?\d*\z/
          self.errors.add(:contacts, 'должны быть представлены телефоном или адресом электронной почты')
        end
      end
    end
  end

  def skills
    Skill.where(id: self.skill_ids)
  end

  def best_vacancies
    skills = self.skill_ids.compact
    skills = [0] if !skills || skills.empty? 
    Vacancy.by_skills(skills).by_wage.active
  end

  def other_vacancies
    skills = self.skill_ids.compact
    skills = [0] if !skills || skills.empty?     
    Vacancy.active.
      select("vacancies.*, count(skills.id) common_skill_count, string_agg(skills.name, ', ') skill_names").
      joins("inner join skills on skills.id = ANY(array_remove(skill_ids, NULL) & ARRAY#{skills})").group('vacancies.id').
      having("count(skills.id) between 1 and icount(array_remove(skill_ids, NULL))-1").order('count(skills.id) desc').by_wage
  end

end
