class Skill < ActiveRecord::Base
  validates :name, presence: true, uniqueness: true

  scope :like_name, ->(name) {where("lower(name) like lower('%' || ? || '%')", name)}
end
