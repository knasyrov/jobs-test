class CandidatesController < ApplicationController
  inherit_resources

protected
  def candidate_params
    params.require(:candidate).permit(:name, :salary, :is_active, :contacts, skill_ids: [], new_skills: [])
  end
end
