class SkillsController < ApplicationController
  def index
    @skills = Skill.all
  end

  def show
    @skill = Skill.find(params[:id])
    @candidates = Candidate.by_skill(@skill.id)
    @vacancies = Vacancy.by_skill(@skill.id)
  end

  def search
    @skills = Skill.like_name(params[:query])
    render json: @skills.map{|s| {name: s.name, id: s.id}}
  end
end
