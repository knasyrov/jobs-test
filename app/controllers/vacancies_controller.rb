class VacanciesController < ApplicationController
  inherit_resources

protected
  def vacancy_params
    if action_name == 'create'
      params.require(:vacancy).permit(:name, :wage, :contacts, :validity, skill_ids: [], new_skills: [])
    else
      params.require(:vacancy).permit(:name, :wage, :contacts, :active_until, skill_ids: [], new_skills: [])
    end
  end
end
